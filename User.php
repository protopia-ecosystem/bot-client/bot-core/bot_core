<?php
//https://stackoverflow.com/questions/2715465/converting-a-php-array-to-class-variables

class User
{

    private $_data;

    public function __construct(){
        $properties = $_SESSION["user"];
        $this->_data = $properties;
    }

    // magic methods!
    public function __set($property, $value){
        $_SESSION["user"][$property] = $value;
        return $this->_data[$property] = $value;
    }

    public function __get($property){
        return array_key_exists($property, $this->_data)
            ? $this->_data[$property]
            : null
            ;
    }

}