<?php

function _callback_handleEvent() {

    if (!isset($_REQUEST)) {
        exit;
    }

    $event = _callback_getEvent();

    //проверку секретного ключа

    try {

        switch ($event['type']) {
            //Подтверждение сервера
            case CALLBACK_API_EVENT_CONFIRMATION:
                _callback_handleConfirmation();
                break;

            //Обработка логики
            case CALLBACK_API_EVENT_MESSAGE_NEW:
                _callback_handleMessageNewLogic($event['object']);
                break;

            default:
                _callback_response('Unsupported event');
                break;
        }
    } catch (Exception $e) {
        Logger::log_error($e);
    }

    _callback_okResponse();
}

function _callback_handleMessageNewLogic($data) {

    pe_session_start("user_" . $data["user_id"]);

    $graphql_protopia = new graphql_protopia(ECOSYSTEM_SERVER_URI, ECOSYSTEM_CLIENT_ID, ECOSYSTEM_CLIENT_URI, ECOSYSTEM_CLIENT_SECRET, "vk", $data["user_id"]);

//  Logger::log_msg("user");


    try {
        Router::current($data);
    } catch (Exception $e) {
        Logger::log_error($e);
    }

    pe_session_write("user_" . $data["user_id"]);

    _callback_okResponse();
}

function _callback_getEvent() {
  return json_decode(file_get_contents('php://input'), true);
}

function _callback_handleConfirmation() {
  _callback_response(CALLBACK_API_CONFIRMATION_TOKEN);
}

function _callback_okResponse() {
  _callback_response('ok');
}

function _callback_response($data) {
  echo $data;
  exit();
}


