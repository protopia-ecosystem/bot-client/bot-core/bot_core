<?php

function pe_session_start($session_id) {
    $session_file = BOT_SESSION_DIRECTORY . "/" . $session_id . ".json";
    $_SESSION = json_decode(file_get_contents($session_file), true);
}

function pe_session_write($session_id) {
    $session_file = BOT_SESSION_DIRECTORY . "/" . $session_id . ".json";
    file_put_contents($session_file, json_encode($_SESSION));
}
